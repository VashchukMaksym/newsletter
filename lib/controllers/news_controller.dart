import 'dart:math';

import 'package:get/get.dart';
import 'package:newsletter/model/news.dart';
import 'package:newsletter/internal/cache/cache.dart';

class NewsController extends GetxController {
  final _local = Cache();
  bool header = true;

  Map<int, NewsModel> listNews = {};

  DateTime date = DateTime.now();

  NewsController() {
    for (var i = 0; i < 6; i++) listNews[i] = createExampleNews(i);
  }

  NewsModel createExampleNews(int newsID) {
    final news = NewsModel();
    news.id = newsID;
    news.commentsCount = Random().nextInt(90) + 10;
    news.likesCount = Random().nextInt(90) + 10;
    news.author.id = 1;
    news.author.childAgeWeeks = 62;
    news.author.name = "Anna Ivanova";
    news.author.avatar = "assets/images/avatar.png";
    news.favorites = false;
    news.title = "Погодки!";
    news.text =
        "Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или...";
    news.topic = "От рождения до года";
    news.image = (newsID + 1) % 2 == 0 ? "assets/images/image.png" : "";
    news.date = DateTime.now();

    return news;
  }

  Map<int, NewsModel> getNews() {
    return listNews;
  }

  void closeHeader() {
    header = false;
    update(["home"]);
  }

  bool isLikeNews(int newsID) {
    return _local.isLikeNews(newsID);
  }

  bool isFavoriteNews(int newsID) {
    return _local.isFavoriteNews(newsID);
  }

  void onLikeClicked(int newsID, bool liked) {
    _local.saveLikeNews(newsID, liked);
    if (liked)
      listNews[newsID]!.likesCount++;
    else
      listNews[newsID]!.likesCount--;

    update(["news"]);
  }

  void onFavoriteClicked(int newsID, bool favorited) {
    _local.saveFavoriteNews(newsID, favorited);
    update(["news"]);
  }
}
