import 'package:newsletter/model/user.dart';

class NewsModel {
  int id = 0;
  int commentsCount = 0;
  int likesCount = 0;
  UserModel author = UserModel();
  bool favorites = false;
  String title = "";
  String text = "";
  String topic = "";
  String image = "";
  DateTime date = DateTime.now();

  NewsModel();

  bool isExist() {
    return this.id != 0;
  }
}
