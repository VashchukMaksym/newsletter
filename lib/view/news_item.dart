import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';

import 'package:newsletter/controllers/news_controller.dart';

class NewsItem extends StatelessWidget {
  final NewsController controller = Get.put(NewsController());
  final int newsID;

  NewsItem({
    required this.newsID,
  });

  Widget _buildLikeButton(BuildContext context, bool isLiked) {
    return GetBuilder<NewsController>(
      builder: (controller) {
        var icon = Icons.favorite_border;
        var color = Colors.grey;

        if (isLiked) {
          icon = Icons.favorite;
          color = Colors.red;
        }

        return GestureDetector(
          onTap: () {
            controller.onLikeClicked(newsID, !isLiked);
          },
          child: Icon(
            icon,
            color: color,
          ),
        );
      },
    );
  }

  Widget _buildFavoriteButton(BuildContext context, bool isFavorite) {
    return GetBuilder<NewsController>(
      builder: (controller) {
        var icon = Icons.star_border;
        var color = Colors.grey;

        if (isFavorite) {
          icon = Icons.star;
          color = Colors.red;
        }

        return GestureDetector(
          onTap: () {
            controller.onFavoriteClicked(newsID, !isFavorite);
          },
          child: Icon(
            icon,
            color: color,
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    var news = controller.getNews()[newsID];
    var childAgeWeeks = news!.author.childAgeWeeks;

    return GetBuilder<NewsController>(
        id: "news",
        builder: (controller) {
          return Container(
            padding: EdgeInsets.only(bottom: 10),
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Color.fromRGBO(220, 220, 220, 1),
            ),
            child: Container(
              padding: EdgeInsets.only(top: 20),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(width: 20),
                      Image.asset(news.author.avatar, width: 25, height: 25),
                      SizedBox(width: 10),
                      Text(
                        news.author.name,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(width: 10),
                      if (childAgeWeeks > 4)
                        Container(
                          padding: EdgeInsets.only(
                              left: 6, right: 6, bottom: 3, top: 3),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(247, 93, 111, 1),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Row(
                            children: [
                              Text(
                                childAgeWeeks > 52
                                    ? (childAgeWeeks / 52).round().toString() +
                                        "г " +
                                        ((childAgeWeeks % 52) ~/ 4).toString() +
                                        "м"
                                    : (childAgeWeeks / 4).round().toString() +
                                        "м",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ],
                          ),
                        ),
                      SizedBox(width: 5),
                      if (childAgeWeeks % 4 > 0)
                        Container(
                          padding: EdgeInsets.only(
                              left: 6, right: 6, bottom: 3, top: 3),
                          decoration: BoxDecoration(
                            color: Color.fromRGBO(32, 179, 198, 1),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Row(
                            children: [
                              Text(
                                (childAgeWeeks % 4).toString() + "н",
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ],
                          ),
                        )
                    ],
                  ),
                  if (news.image != "") SizedBox(height: 10),
                  if (news.image != "")
                    Image.asset(news.image,
                        width: MediaQuery.of(context).size.width),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      SizedBox(width: 20),
                      Expanded(
                        child: Text(
                          news.title,
                          style: TextStyle(
                            fontSize: 22,
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: [
                      SizedBox(width: 20),
                      Expanded(
                        child: Text(
                          news.text,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                      ),
                      SizedBox(width: 20),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SizedBox(width: 20),
                          Text(
                            DateFormat('dd MMMM yyyy')
                                .format(DateTime.fromMillisecondsSinceEpoch(
                                    news.date.millisecondsSinceEpoch))
                                .toString(),
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.arrow_right_alt_sharp,
                            color: Colors.grey,
                          ),
                          Text(
                            news.topic,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          SizedBox(width: 20),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width - 40,
                    height: 1,
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(245, 245, 245, 1),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          SizedBox(width: 20),
                          _buildLikeButton(
                              context, controller.isLikeNews(newsID)),
                          SizedBox(width: 10),
                          Container(
                            width: 20,
                            child: Text(
                              news.likesCount.toString(),
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Material(
                            type: MaterialType.transparency,
                            child: IconButton(
                              padding: EdgeInsets.zero,
                              splashRadius: 20,
                              icon: Icon(
                                Icons.mode_comment_outlined,
                                color: Colors.grey,
                              ),
                              onPressed: () {},
                            ),
                          ),
                          Container(
                            width: 20,
                            child: Text(
                              news.commentsCount.toString(),
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.grey,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          SizedBox(width: 10),
                          Material(
                            type: MaterialType.transparency,
                            child: IconButton(
                              padding: EdgeInsets.zero,
                              splashRadius: 20,
                              icon: Icon(
                                Icons.share_outlined,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                Share.share(news.text);
                              },
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          _buildFavoriteButton(
                              context, controller.isFavoriteNews(newsID)),
                          SizedBox(width: 20),
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }
}
