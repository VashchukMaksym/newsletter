import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:newsletter/controllers/news_controller.dart';
import 'package:newsletter/view/news_item.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatelessWidget {
  final NewsController controller = Get.put(NewsController());
  final PageController pages = PageController(initialPage: 0);

  Widget _buildPage(BuildContext context) {
    var exampleNews = controller.getNews();

    return GetBuilder<NewsController>(
      builder: (controller) {
        return Column(
          children: [
            Expanded(
                child: ListView(
              children: [
                if (controller.header)
                  Container(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: GestureDetector(
                      onTap: () {},
                      child: Container(
                        padding: EdgeInsets.only(left: 20, bottom: 30),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(32, 179, 198, 1),
                        ),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.arrow_right_alt_sharp,
                                      color: Color.fromRGBO(255, 255, 255, 0.8),
                                    ),
                                    Text(
                                      "Тема дня",
                                      style: TextStyle(
                                        fontSize: 12,
                                        color:
                                            Color.fromRGBO(255, 255, 255, 0.8),
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ],
                                ),
                                Material(
                                  type: MaterialType.transparency,
                                  child: IconButton(
                                    padding: EdgeInsets.zero,
                                    splashRadius: 20,
                                    icon: Icon(
                                      Icons.close,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      controller.closeHeader();
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "А как Вы с любимым ласково называете друг друга?",
                                    style: TextStyle(
                                      fontSize: 22,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                if (!controller.header) SizedBox(height: 10),
                for (var i = 0; i < exampleNews.length; i++)
                  NewsItem(newsID: i),
              ],
            )),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NewsController>(
        id: "home",
        builder: (controller) {
          return DefaultTabController(
            length: 3,
            child: Scaffold(
              backgroundColor: Color.fromRGBO(220, 220, 220, 1),
              appBar: AppBar(
                backgroundColor: Colors.white,
                leading: Container(
                  padding: EdgeInsets.all(11.0),
                  child: Container(
                    padding: EdgeInsets.all(9.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(32, 179, 198, 1),
                    ),
                    child: SvgPicture.asset(
                      "assets/images/ico.svg",
                      color: Colors.white,
                    ),
                  ),
                ),
                title: Text(
                  "Новости",
                  style: TextStyle(
                    fontSize: 22,
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                actions: <Widget>[
                  IconButton(
                    splashRadius: 20,
                    icon: Icon(
                      Icons.location_on_outlined,
                      color: Colors.black,
                    ),
                    onPressed: () {},
                  ),
                  IconButton(
                    splashRadius: 20,
                    icon: Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    onPressed: () {},
                  )
                ],
                bottom: TabBar(
                    indicatorWeight: 5,
                    indicatorColor: Color.fromRGBO(32, 179, 198, 1),
                    labelColor: Colors.black,
                    unselectedLabelColor: Colors.grey,
                    labelStyle: TextStyle(fontWeight: FontWeight.bold),
                    tabs: [
                      Tab(text: "Новые"),
                      Tab(text: "Популярные"),
                      Tab(text: "Подписки"),
                    ],
                    onTap: (index) {
                      onTapHandler(index);
                    }),
              ),
              body: PageView(
                physics: NeverScrollableScrollPhysics(),
                scrollDirection: Axis.horizontal,
                controller: pages,
                children: [
                  _buildPage(context),
                  _buildPage(context),
                  _buildPage(context),
                ],
              ),
            ),
          );
        });
  }

  void onTapHandler(int index) {
    switch (index) {
      case 0:
        pages.animateToPage(
          0,
          curve: Curves.easeIn,
          duration: Duration(milliseconds: 300),
        );
        break;
      case 1:
        pages.animateToPage(
          1,
          curve: Curves.easeIn,
          duration: Duration(milliseconds: 300),
        );
        break;
      case 2:
        pages.animateToPage(
          2,
          curve: Curves.easeIn,
          duration: Duration(milliseconds: 300),
        );
        break;
    }
  }
}
