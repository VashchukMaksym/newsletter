import 'package:get_storage/get_storage.dart';

class LocalStorage {
  final _box = GetStorage();

  void writeNumber(String key, num val) async {
    _box.write(key, val);
  }

  num readNumber(String key, {num defaultValue = 0}) {
    var i = _box.read(key);
    if (i == null) {
      return defaultValue;
    }
    return i;
  }

  void writeString(String key, String val) async {
    _box.write(key, val);
  }

  String readString(String key, {String defaultValue = ""}) {
    var i = _box.read(key);
    if (i == null) {
      return defaultValue;
    }
    return i;
  }

  void writeBool(String key, bool val) async {
    _box.write(key, val);
  }

  bool readBool(String key, {bool defaultValue = false}) {
    var i = _box.read(key);
    if (i == null) {
      return defaultValue;
    }
    return i;
  }

  void remove(String key) {
    _box.remove(key);
  }
}
