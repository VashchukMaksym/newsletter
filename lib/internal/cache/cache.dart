import 'package:path_provider/path_provider.dart';
import 'primitive.dart';

class Cache extends LocalStorage {
  static final Cache _instance = Cache._privateConstructor();

  String documentsDir = "";

  factory Cache() {
    return _instance;
  }

  Cache._privateConstructor() {
    getApplicationDocumentsDirectory()
        .then((value) => documentsDir = value.path);
  }

  bool isLikeNews(int newsID) {
    return readBool("like_" + newsID.toString());
  }

  void saveLikeNews(int newsID, bool liked) {
    writeBool("like_" + newsID.toString(), liked);
  }

  bool isFavoriteNews(int newsID) {
    return readBool("favorite_" + newsID.toString());
  }

  void saveFavoriteNews(int newsID, bool favorited) {
    writeBool("favorite_" + newsID.toString(), favorited);
  }
}
